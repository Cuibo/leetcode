#include <vector>
#include <iostream>

using namespace std;
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if(nums.size() == 0){
            return 0;
        } else{
            int before = nums[0];
            int removedtime =0;
            for (int i = 1; i < nums.size(); i++){
                if(before == nums[i]){
                    nums.erase(nums.begin()+i);
                    i--;
                }else{
                    before = nums[i];
                }
            }
            return nums.size();

        }
    }
};

int main(){
    Solution test;

    vector<int> testcase = {1,2,3,4,5,5};
    for (int i = 0; i < testcase.size(); i++){
        cout << testcase[i]<< ", ";
    }
    cout <<endl <<  "after remove, the vector is " << endl;
    for (int i = 0; i < testcase.size(); i++){
        cout << testcase[i]<< ", ";
    }
}