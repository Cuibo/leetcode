//
// Created by Bo Cui on 4/27/19.
//
#include<vector>


class Solution {
public:
    int searchInsert(std::vector<int>& nums, int target) {
        int index = 0;
        bool breaked = false;
        if(nums.size() == 0){
            index = 0;
        }else{
            for (int i = 0; i < nums.size(); i++){
                if(nums[i] == target){
                    index = i;
                    breaked = true;
                    break;
                } else if (nums[i] > target){
                    index = i;
                    breaked = true;
                    break;
                }
            }
        }
        if (breaked){
            return index;
        } else{
            return nums.size();
        }

    }

};